---
date: 2023-04-09T13:18:08-05:00
description: "Introducing SmolTech"
featured_image: "/img/logo.png"
tags: ["SmolTech"]
title: "Introducing SmolTech LLC"
---

I'd like to first start my introducing myself.

My name is Gregory Boyce, and I'm an accomplished manager of software engineering teams. I spent most of my career working at Akamai Technologies, which has been named one of the best companies in Massachusetts to work at.

During my time there, I had opportunities to learn a diverse skillset, working with some of the best talent in the world.  I helped lead server operations, security operations and spent my last five years there leading a team responsible for their custom Linux distribution and related management tools.

In 2018, I decided to take another path in order to find ways to use my existing experience with computing power and Open Source software in order to help build a better future.  After a year in research and planning, I relocated to Worcester Massachusetts and attempted to open a community space to help people learn to use Open Source tools to solve their own problems.

Unforunately, Auburndale Technology was a victim of COVID, and I went back into the industry to modernize my skillsets.

Now, I'm introducing my second attempt.

Introducing SmolTech LLC.

We are focused on building community clouds, Open Source software, and enabling a new distributed economy.

How can I help you succeed?
