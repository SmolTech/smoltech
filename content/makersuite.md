---
title: "Makersuite"

description: "Software For Makerspaces"
cascade:
  featured_image: '/img/SmolTech.png'
---

Makersuite is software for managing Maker Spaces, including membership management, ecommerce and a locally managed gig board.

{{< rawhtml >}}
<!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/smoltech/30min" style="min-width:320px;height:700px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js" async></script>
<!-- Calendly inline widget end -->
{{< /rawhtml >}}

